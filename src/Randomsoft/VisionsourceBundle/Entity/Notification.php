<?php
// src/Randomsoft/VisionsourceBundle/Entity/Notification.php

namespace Randomsoft\VisionsourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="Randomsoft\VisionsourceBundle\Entity\Repository\NotificationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Notification
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", length=25000)
     */
    protected $body;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $creator;
    
    /**
     * @var \DateTime
     * @ORM\Column(type="string", nullable=false)
     */
    protected $createdAt;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $signed;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Title
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set body
     *
     * @param string $body
     * @return Body
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * Set creator
     *
     * @param string $creator
     * @return Creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return string 
     */
    public function getCreator()
    {
        return $this->creator;
    }
    
    /**
     * @return boolean 
     */
    public function isSigned()
    {
        return $this->signed;
    }
    
    /**
     * @return boolean 
     */
    public function isActive()
    {
        return $this->active;
    }
    
    /**
     * Set signed
     *
     * @param boolean $signed
     * @return Signed
     */
    public function setSigned($signed)
    {
        $this->signed = $signed;
        
        return $this;
    }
    
     /**
      * Set active
      *
      * @param boolean $active
      * @return Active
      */
    public function setActive($active)
    {
        $this->active = $active;
        
        return $this;
    }
    
    /** 
     *  @ORM\PrePersist 
     */
    public function doStuffOnPrePersist()
    {
        $this->createdAt = date('d-m-Y H:i:s');
    }
    
     /** 
     *  @ORM\PreUpdate 
     */
    public function doStuffOnPreUpdate()
    {
        $this->createdAt = date('d-m-Y H:i:s');
    }
    
    
    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
