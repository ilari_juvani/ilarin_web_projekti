<?php
// src/Randomsoft/VisionsourceBundle/Entity/Mode.php

namespace Randomsoft\VisionsourceBundle\Entity;


class Mode
{
    protected $modeChoice;
    
    protected $interval;
    
    public function getModeChoice()
    {
        return $this->modeChoice;
    }

    public function setModeChoice($modeChoice)
    {
        $this->modeChoice = $modeChoice;
    }
    
    public function getInterval()
    {
        return $this->interval;
    }

    public function setInterval($interval)
    {
        $this->interval = $interval;
    }

}