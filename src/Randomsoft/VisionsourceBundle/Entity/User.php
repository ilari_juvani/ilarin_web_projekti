<?php

// src/VisionsourceBundle/Entity/User.php

namespace Randomsoft\VisionsourceBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Util\SecureRandom;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

//'user' is a reserverd keyword in SQL standard. If a reserved keyword is used, it has to be escaped with `` 

/**
 * @ORM\Entity(repositoryClass="Randomsoft\VisionsourceBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
   /**
    * @Assert\Length(
    *      max = 200,
    *      maxMessage = "Description cannot contain more than {{ limit }} characters!"
    * )
    */
    protected $description;
    
     public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @ORM\ManyToMany(targetEntity="Randomsoft\VisionsourceBundle\Entity\Group", inversedBy="users")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;
    
    
     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageName;
    

    /**
     * Set imageName
     *
     * @param string $imageName
     * @return User
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }
    
    /**
     * Get imageName
     *
     * @return string 
     */
    public function getImageName()
    {
        return $this->imageName;
    }
    
    

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageCroppedPath;
    

    /**
     * Set imageCroppedPath
     *
     * @param string $imageCroppedPath
     * @return User
     */
    public function setImageCroppedPath($imageCroppedPath)
    {
        $this->imageCroppedPath = $imageCroppedPath;

        return $this;
    }
    
    /**
     * Get imageCroppedPath
     *
     * @return string 
     */
    public function getImageCroppedPath()
    {
        return $this->imageCroppedPath;
    }
    
   
    
    public function getAbsolutePath() {
        return null === $this->imageCroppedPath ? null : $this->getUploadRootDir().'/'.$this->imageCroppedPath;
    }

    
    
    public function getUploadRootDir() {
        // absolute path to your directory where images must be saved
        //return __DIR__.'/../../../../web/bundles/randomsoftvisionsource/'.$this->getUploadDir();
        return str_replace('\\', '\\\\', __DIR__.'/../../../../web/'.$this->getUploadDir());
    }
    
    
    /**
     * Specifies where in the /web directory profile pic uploads are stored
     * 
     * @return string
     */
    public function getUploadDir() {
        // the type param is to change these methods at a later date for more file uploads
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/user';
    }
    
    
    
    public function getWebPath() {
        return null === $this->imageCroppedPath ? null : '/'.$this->getUploadDir().'/'.$this->imageCroppedPath;
    }
    
   
   
    public function getExpiresAt() {
        return $this->expiresAt;
    }
    
    public function getCredentialsExpireAt() {
        return $this->credentialsExpireAt;
    }
    
    /**
     * @var \DateTime
     * @ORM\Column(type="string", nullable=true)
     */
    protected $latestReadNotification;
    
    public function getLatestReadNotification() {
        return $this->latestReadNotification;
    }
    
    public function setLatestReadNotification(\DateTime $date) 
    {
        $this->latestReadNotification = $date;
        
        return $this;
    }
    
    /** 
     *  @ORM\PrePersist 
     */
    public function doStuffOnPrePersist()
    {
        $this->latestReadNotification = null;
    }
    
    
    public function __construct()
    {
        parent::__construct();
        
    }
    
}