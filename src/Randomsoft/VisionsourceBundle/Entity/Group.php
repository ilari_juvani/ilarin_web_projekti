<?php


namespace Randomsoft\VisionsourceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_group")
 */
class Group extends BaseGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="Randomsoft\VisionsourceBundle\Entity\User", mappedBy="groups")
     * 
     */
     protected $users;
    
    public function getUsers()
    {
        return $this->users ?: $this->users = new ArrayCollection();
    }
    
    public function addUser($user)
    {
        if (!$this->getUsers()->contains($user)) {
            $this->getUsers()->add($user);
        }
        return $this;
    }
}