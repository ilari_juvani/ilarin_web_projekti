<?php
// src/Randomsoft/VisionsourceBundle/Entity/Phrase.php

namespace Randomsoft\VisionsourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="phrase")
 * @ORM\Entity(repositoryClass="Randomsoft\VisionsourceBundle\Entity\Repository\PhraseRepository")
 */
class Phrase
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $phrase;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $creator;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phrase
     *
     * @param string $phrase
     * @return Phrase
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;

        return $this;
    }

    /**
     * Get phrase
     *
     * @return string 
     */
    public function getPhrase()
    {
        return $this->phrase;
    }
    
    /**
     * Set creator
     *
     * @param string $creator
     * @return Creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return string 
     */
    public function getCreator()
    {
        return $this->creator;
    }
}
