<?php

namespace Randomsoft\VisionsourceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;



class Control
{
   /** 
     * @Assert\All({
     *   @Assert\Length(
     *          min = 2,
     *          max = 50,
     *          minMessage = "Word must be at least {{ limit }} characters long",
     *          maxMessage = "Word cannot be longer than {{ limit }} characters"
     *      )
     * })
     */
    public $words = array();
    
    public function getWords()
    {
        return $this->words;
    }
    
    /*
 
    public function setWords(array $words)
    {
        $this->words = $words;
    }
    
    */
    
    
    
    
    /** 
     * @Assert\All({
     *   @Assert\Length(
     *          min = 5,
     *          max = 250,
     *          minMessage = "Phrase must be at least {{ limit }} characters long",
     *          maxMessage = "Phrase cannot be longer than {{ limit }} characters"
     *      )
     * })
     */
    public $phrases = array();
    
    public function getPhrases()
        {
            return $this->phrases;
        }

   /*
    public function setPhrases(array $phrases)
        {
            $this->phrases = $phrases;
        }
    */
    
  
    public $picture = array();
    
    public function __construct()
    {

    }

    
    public function getPicture()
    {
        return $this->picture;
    }
    
    /*
    
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }
    */
    
    public function returnArray()
    {
        return array(
                'words' => $this->words,
                'phrases' => $this->phrases
            );
    }
    

}