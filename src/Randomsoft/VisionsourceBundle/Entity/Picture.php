<?php
//src/Randomsoft/VisionsourceBundle/Entity/Picture.php

namespace Randomsoft\VisionsourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity
 * @ORM\Table(name="picture")
 * @ORM\Entity(repositoryClass="Randomsoft\VisionsourceBundle\Entity\Repository\PictureRepository")
 */
class Picture
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
   
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
      $this->name = $name;  
    }
      
        
    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads';
    }
    
     /**
     * @ORM\Column(type="string")
     */
    protected $creator;
    
    /**
     * Set creator
     *
     * @param string $creator
     * @return Creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return string 
     */
    public function getCreator()
    {
        return $this->creator;
    }
    
   
    protected $file;

    
    public function setFile($file)
    {
        $this->file = $file;
    }

   
    public function getFile()
    {
        return $this->file;
    }
    
    
    public function generateName($mimeType, $fileName)
    {
     //the uploaded image file is given a randomly generated, 12 character long name
        
        if($mimeType === 'image/jpeg'){
            $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $length = 12;
            for($i = 0; $i < $length; $i++)
            {
                $fileName .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            
            $fileName .= '.jpg';
            
            return $fileName;

        }elseif($mimeType === 'image/png'){
            $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $length = 12;
            for($i = 0; $i < $length; $i++)
            {
                $fileName .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            
            $fileName .= '.png';
            
            return $fileName;

        }else{
            return 'Filetype error';
        }   
           
    }
    
   
    
    public function upload()
    {
        $picData = $this->getFile();
        // the file property can be empty if the field is not required
        if (null === $picData) {
            return;
        }
        
        $mimeType = $picData->getMimeType();
        
        generateName($mimeType);

        

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move($this->getUploadRootDir(), $filename);

        // set the path property to the filename where you've saved the file
        $this->path = $filename; //$this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }
}
