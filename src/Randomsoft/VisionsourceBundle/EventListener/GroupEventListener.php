<?php

namespace Randomsoft\VisionsourceBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GroupEvent;
use FOS\UserBundle\Event\FilterGroupResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;



class GroupEventListener extends Controller implements EventSubscriberInterface 
{
    
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::GROUP_CREATE_SUCCESS => 'onGroupCreate',
            FOSUserEvents::GROUP_EDIT_SUCCESS => 'onGroupEdit'
        );
    }
    
    
    public function onGroupCreate(FormEvent $event)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        
        $group = $event->getForm()->getData();
        
        $groupName = strtoupper($group->getName());
        
        //$group->addRole('ROLE_'.$groupName.'_FOUNDER');
        
        $groupRoles = array('ROLE_'.$groupName.'_FOUNDER', 'ROLE_'.$groupName.'_MEMBER');
        
        $group->setRoles($groupRoles);
            
        $user->addRole('ROLE_'.$groupName.'_FOUNDER');
        
        
    }
    
    
    public function onGroupEdit(FormEvent $event)
    {
        
        $group = $event->getForm()->getData();
        
        $groupName = strtoupper($group->getName());
        
        $user = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:User')
        ->findBy(array('username' => 'mcmuffinz'));
        
        $user[0]->addRole('ROLE_'.$groupName.'_MEMBER');
        
        
    }
    
    
}
