<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class UserPhraseController extends Controller
{
    public function userPhrasesAction(Request $request)
    {
         $user = $this->getUser();
         $userPhrases = $this->getDoctrine()
            ->getRepository('RandomsoftVisionsourceBundle:Phrase')
            ->findBy(array('creator' => $user->getUsername()));

         if($request->isXmlHttpRequest()) {
             
             $phraseID = $request->request->get('phrase_id');
             $newPhrase = $request->request->get('phrase_content');
             
             if($newPhrase === NULL) {
                 $template = $this->forward('RandomsoftVisionsourceBundle:UserPhrase:deleteUserPhrase', array(
                    'phraseID' => $phraseID
                 ));
             }else{
                  $template = $this->forward('RandomsoftVisionsourceBundle:UserPhrase:editUserPhrase', array(
                    'phraseID' => $phraseID,
                    'newPhrase' => $newPhrase
                )); 
             }
             
          $response = new Response($template);
          return $response;

         }else{
             
            return $this->render('FOSUserBundle:Profile:show_phrase_content.html.twig', array(
                'phrases' => $userPhrases
            ));

        }
     
    }
    
    public function editUserPhraseAction($phraseID, $newPhrase)
    {
        $em = $this->getDoctrine()->getManager(); 
        $phrase = $em->getRepository('RandomsoftVisionsourceBundle:Phrase')->find($phraseID);
        $phrase->setPhrase($newPhrase);
        $em->flush();
        
        $this->addFlash(
            'phrase',
            'Phrase edited successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:phrase_content_changed.html.twig');
    }
    
    public function deleteUserPhraseAction($phraseID)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $phrase = $em->getRepository('RandomsoftVisionsourceBundle:Phrase')->find($phraseID);       
        $em->remove($phrase);  
        $em->flush();
        
        $this->addFlash(
            'phrase',
            'Phrase deleted successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:phrase_content_changed.html.twig');
    }
                 
}