<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Randomsoft\VisionsourceBundle\Form\ProfileEditType;


class ProfileController extends Controller
{
     /**
     * Show the user
     */
    public function showAction()
    {
        $user = $this->getUser();
        
        
        
        $userPhrases = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Phrase')
        ->findBy(array('creator' => $user->getUsername()));
        
        
        $userPictures = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Picture')
        ->findBy(array('creator' => $user->getUsername()));
        
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        return $this->render('FOSUserBundle:Profile:show.html.twig', array(
            'user' => $user,
            'phrases' => $userPhrases,
            'pictures' => $userPictures 
        ));
    }
    
    public function settingsAction(Request $request)
    {
        $user = $this->getUser();
        
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
        $profile_edit_form = $this->createForm(new ProfileEditType(), $user);
        
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $password_form = $formFactory->createForm();
        
        if ($request->isMethod('POST')) {
            
                $profile_edit_form->handleRequest($request);
                $formData = $profile_edit_form->getData();
                //$files = $request->files->get('file');
                
                $validator = $this->get('validator');
                $errors = $validator->validate($user);

                if (count($errors) > 0) {
                    return $this->render('FOSUserBundle:Profile:profile_settings.html.twig', array(
                        'user' => $user,
                        'password_form' => $password_form->createView(),
                        'profile_edit_form' => $profile_edit_form->createView(),
                        'errors' => $errors
                    ));
                    
                    
                }else{
                    $em = $this->getDoctrine()->getManager();
                    
                    
                            /*
                             $nameInit = '';
                             $picture = new Picture();
                             $originalName = $uploadedFile->getClientOriginalName();
                             $mimeType = $uploadedFile->getClientMimeType();
                             $fileName = $picture->generateName($mimeType, $nameInit);
                             $directory = __DIR__.'/../../../../web/bundles/randomsoftvisionsource/uploads/';
                             
                             $picture->setFile($uploadedFile);
                             $picture->setName($fileName);
                             $picture->setCreator($user->getUsername());
                             $file = $uploadedFile->move($directory, $fileName);
                             $em->persist($picture);
                             $em->flush();
                             $em->clear();
                             */
                            
                          $em->persist($user);
                          $em->flush();
                          $em->clear();  
                    
                }
            
            
        }
        
        return $this->render('FOSUserBundle:Profile:profile_settings.html.twig', array(
            'user' => $user,
            'password_form' => $password_form->createView(),
            'profile_edit_form' => $profile_edit_form->createView()
        ));
    }
    
    
   
    public function userWordsAction(Request $request)
    {
         $user = $this->getUser();
         $userWords = $this->getDoctrine()
            ->getRepository('RandomsoftVisionsourceBundle:Word')
            ->findBy(array('creator' => $user->getUsername()));

         if($request->isXmlHttpRequest()) {
             //$editedData = $request->get('inputData');
             //$editedData = array();
             //$content = $this->get("request")->getContent();
             //$editedData = json_decode($content, true);
             
             $wordID = $request->request->get('id');
             $newWord = $request->request->get('content');
             
             /*
             return $this->render('FOSUserBundle:Profile:show_word_content.html.twig', array(
                 'words' => $userWords, 
                 'editedData' => $editedData
            ));
             */
             
             if($newWord === NULL) {
                 $template = $this->forward('RandomsoftVisionsourceBundle:Profile:deleteUserWord', array(
                    'wordID' => $wordID
                 ));
             }else{
                  $template = $this->forward('RandomsoftVisionsourceBundle:Profile:editUserWord', array(
                    'wordID' => $wordID,
                    'newWord' => $newWord
                )); 
             }
             
            

            //$json = json_encode($template);
            //$response = new Response($json, 200);
            //$response->headers->set('Content-Type', 'application/json');
            $response = new Response($template);
    
            return $response;

         }else{
             
            return $this->render('FOSUserBundle:Profile:show_word_content.html.twig', array(
                'words' => $userWords
            ));

        }
        
        
    
    }
    
    public function editUserWordAction($wordID, $newWord)
    {
        $em = $this->getDoctrine()->getManager(); 
        $word = $em->getRepository('RandomsoftVisionsourceBundle:Word')->find($wordID);
        $word->setWord($newWord);
        $em->flush();
        
        $this->addFlash(
            'word',
            'Word edited successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:word_content_changed.html.twig');
    }
    
    public function deleteUserWordAction($wordID)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $word = $em->getRepository('RandomsoftVisionsourceBundle:Word')->find($wordID);       
        $em->remove($word);  
        $em->flush();
        
        $this->addFlash(
            'word',
            'Word deleted successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:word_content_changed.html.twig');
    }
    
    
    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('FOSUserBundle:Profile:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /*
    public function addProfilePictureAction(Request $request)
    {
        
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
        
                
                
                $files = $request->files->get('file');
                $validator = $this->get('validator');
                $errors = $validator->validate($control);

                if (count($errors) > 0) {
                    return $this->render('RandomsoftVisionsourceBundle:Control:control.html.twig', array(
                        'form' => $form->createView(),
                        'errors' => $errors
                    ));
                }else{
                    
                    
                    
                    
                }
        
        
        $em = $this->getDoctrine()->getManager(); 
        $word = $em->getRepository('RandomsoftVisionsourceBundle:Word')->find($wordID);
        $word->setWord($newWord);
        $em->flush();
        
        $this->addFlash(
            'word',
            'Word edited successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:word_content_changed.html.twig');
    }
    */
    
    /**
     * @Route("/profile/delete/{picture_name}", defaults={"picture_name" = 0}, name="delete")
     */
    public function deleteAction($picture_name)
    {
        $picture = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Picture')
        ->findBy(array('name' => $picture_name));
        $em = $this->getDoctrine()->getEntityManager();         
        $em->remove($picture[0]); //entity manager method requires object as a parameter! $picture is an array, so we have to access first index  
        $em->flush();
        
        $this->addFlash(
            'notice',
            'Picture deleted successfully'
        );

        return $this->redirect($this->generateUrl('fos_user_profile_show'));
                                 
    }
                 
}