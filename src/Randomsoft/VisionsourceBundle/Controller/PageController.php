<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Randomsoft\VisionsourceBundle\Entity\Mode;
use Randomsoft\VisionsourceBundle\Form\ModeType;

class PageController extends Controller
{
    public function indexAction(Request $request){
        $mode = new Mode();
        $form = $this->createForm(new ModeType(), $mode, array(
            'action' => $this->generateUrl('RandomsoftVisionsourceBundle_home'),
            'method' => 'POST',
        ));

        //$request->isXmlHttpRequest();
        if ($request->isMethod('POST')) {
            
            $form->handleRequest($request);
            $modeType = $form->getData();
            //$choice = $_POST['mode[modeChoice]'];
            
            if (($form->isSubmitted()) && ($modeType->getModeChoice() == 'word')) {
                
                $response = $this->forward('RandomsoftVisionsourceBundle:Page:word', array(
                    'interval' => $modeType->getInterval()
                ));
                return $response;
                return $this->redirect($this->generateUrl('RandomsoftVisionsourceBundle_home'));
                
            }
            
            if (($form->isSubmitted()) && ($modeType->getModeChoice() == 'phrase')) {
                $response = $this->forward('RandomsoftVisionsourceBundle:Page:phrase', array(
                    'interval' => $modeType->getInterval()
                ));
                return $response;
                return $this->redirect($this->generateUrl('RandomsoftVisionsourceBundle_home'));
            }
            
            if (($form->isSubmitted()) && ($modeType->getModeChoice() == 'picture')) {
                $response = $this->forward('RandomsoftVisionsourceBundle:Page:picture', array(
                    'interval' => $modeType->getInterval()
                ));
                return $response;
                return $this->redirect($this->generateUrl('RandomsoftVisionsourceBundle_home'));
            }
         
        }
      return $this->render('RandomsoftVisionsourceBundle:Page:index.html.twig', array(
            'form' => $form->createView()
        ));
   
    }
    
    public function wordAction($interval){
        $words = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Word')
        ->findAll();
        
        return $this->render('RandomsoftVisionsourceBundle:Page:word.html.twig', array(
            'words' => $words,
            'interval' => $interval
        ));  
    }
    
    public function phraseAction($interval){
        $phrases = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Phrase')
        ->findAll();
        
        return $this->render('RandomsoftVisionsourceBundle:Page:phrase.html.twig', array(
            'phrases' => $phrases,
            'interval' => $interval
        )); 
    }
    
    public function pictureAction($interval){
        
        // We declare a function to shuffle the pictures fetched from database
        // Note: royal slider -plugin also has option to randomize slide order
        function shuffle_assoc($array)
        {
            // Initialize
                $shuffled_array = array();

            // Get array's keys and shuffle them.
                $shuffled_keys = array_keys($array);
                shuffle($shuffled_keys);

            // Create same array, but in shuffled order.
                foreach ( $shuffled_keys AS $shuffled_key ) 
                {
                    $shuffled_array[ $shuffled_key ] = $array[ $shuffled_key ];
                } 
            // Return
                return $shuffled_array;
        }
        
        $pictures = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Picture')
        ->findAll();
        
        $shuffledPictures = shuffle_assoc($pictures); //randomize
        
        return $this->render('RandomsoftVisionsourceBundle:Page:picture.html.twig', array(
            'pictures' => $shuffledPictures,
            'interval' => $interval
        )); 
    }
    
}
