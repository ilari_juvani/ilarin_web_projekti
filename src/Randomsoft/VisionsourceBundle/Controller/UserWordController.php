<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class UserWordController extends Controller
{
    
    public function userWordsAction(Request $request)
    {
        
         $user = $this->getUser();
        
         $userWords = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Word')
        ->findBy(array('creator' => $user->getUsername()));
        
        
         if($request->isXmlHttpRequest()) {
             
             $wordID = $request->request->get('word_id');
             $newWord = $request->request->get('word_content');
            
             
             
             if($newWord === NULL) {
                 $template = $this->forward('RandomsoftVisionsourceBundle:UserWord:deleteUserWord', array(
                    'wordID' => $wordID
                 ));
             }else{
                  $template = $this->forward('RandomsoftVisionsourceBundle:UserWord:editUserWord', array(
                    'wordID' => $wordID,
                    'newWord' => $newWord
                )); 
             }
             
           
          $response = new Response($template);
             
          return $response;
          
        

          }else{
         
            return $this->render('FOSUserBundle:Profile:show_word_content.html.twig', array(
                'words' => $userWords
            ));

        } 
        
        
     
    }
    
    
    public function editUserWordAction($wordID, $newWord)
    {
        $em = $this->getDoctrine()->getManager(); 
        $word = $em->getRepository('RandomsoftVisionsourceBundle:Word')->find($wordID);
        $word->setWord($newWord);
        $em->flush();
        
        $this->addFlash(
            'word',
            'Word edited successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:word_content_changed.html.twig');
    }
    
    public function deleteUserWordAction($wordID)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $word = $em->getRepository('RandomsoftVisionsourceBundle:Word')->find($wordID);       
        $em->remove($word);  
        $em->flush();
        
        $this->addFlash(
            'word',
            'Word deleted successfully'
        );
        
        return $this->render('FOSUserBundle:Profile:word_content_changed.html.twig');
    }
    

    
                 
}