<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationController extends Controller
{
    public function getActiveNotificationAction()
    {
        $notifications = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:Notification')
        ->findBy(array('active' => true));
        
         return $this->render('RandomsoftVisionsourceBundle:Notification:notification.html.twig', array(
            'notifications' => $notifications
        ));
    }
    
   /* 
    public function isNewNotificationsAction()
    {
        $user = $this->getUser();
        $lastLoggedIn = $user->getLastLogin();
        $latestNotification = $user->getLatestReadNotification();
        $em = $this->getDoctrine()->getManager();
        
        if(($lastLoggedIn == null) || ($latestNotification == null)) {
            return $this->render('RandomsoftVisionsourceBundle:Notification:notification.html.twig', array(
                'hasNewNotifications' => true
            ));
        }else{
           $query = $em->createQuery('SELECT n FROM RandomsoftVisionsourceBundle:Notification n WHERE n.createdAt > :last_logged')
               ->setParameter('last_logged', $lastLoggedIn); 
            
            if (($query !== null) && ($query !== '') && ($query !== '[]')) {
                dump($query);
                return $this->render('RandomsoftVisionsourceBundle:Notification:notification.html.twig', array(
                    'hasNewNotifications' => true
                ));
            }else{
                return $this->render('RandomsoftVisionsourceBundle:Notification:notification.html.twig', array(
                    'hasNewNotifications' => false
                ));
            }
        }
            
    }
    
    */
    
    
}