<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterGroupResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseGroupEvent;
use FOS\UserBundle\Event\GroupEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\UserBundle\Controller\GroupController as BaseController;

class GroupController extends BaseController
{
    
    public function showAction($groupName)
    {
        $user = $this->getUser();
        
        $group = $this->findGroupBy('name', $groupName);
        $gid = $group->getID();
        
        $em = $this->getDoctrine()->getManager();
        
        //$groupUsers = $group->getUsers();
        
        $userGroups = $user->getGroups();
        
        $repository = $em->getRepository('RandomsoftVisionsourceBundle:User');
        
        $founder = $repository->findByRole('ROLE_'.strtoupper($groupName).'_FOUNDER');
        
        
        $members = $repository->findByRole('ROLE_'.strtoupper($groupName).'_MEMBER');
        
        /*
        $query = $repository->createQueryBuilder('u')
            ->innerJoin('u.groups', 'g')
            ->where('g.id = :groupId')
            ->setParameter('groupId', $gid)
            ->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
        dump($query);
        */
        
        /*
        $query = $repository->createQueryBuilder('u')
            ->select('u')
            ->addSelect('g')
            ->leftJoin('Randomsoft\VisionsourceBundle\Entity\Group','g','WITH','u.id=g.id')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
        
        dump($query);
       */
        
         $query = $repository->createQueryBuilder('u')
            ->select('u.id')
            ->addSelect('g')
            ->innerJoin('Randomsoft\VisionsourceBundle\Entity\Group','g')
            ->where('g.id = :groupId')
            ->setParameter('groupId', $gid)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        
        
  
        
        /*
        $query = $em->createQuery('SELECT u.id FROM Randomsoft\VisionsourceBundle\Entity\User u WHERE :groupId MEMBER OF u.groups');
        $query->setParameter('groupId', $gid);
        $ids = $query->getResult();
        dump($ids);
        */
        
        $mcmuffinz = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:User')
        ->findBy(array('username' => 'mcmuffinz'));
        
        dump($mcmuffinz[0]);
        
        dump($members);

        return $this->render('FOSUserBundle:Group:show.html.twig', array(
            'group' => $group,
            'founder' => $founder[0]->getUserName()
        ));
    }
    
    
    
    /**
     * Show the new form
     */
    public function newAction(Request $request)
    {
        /** @var $groupManager \FOS\UserBundle\Model\GroupManagerInterface */
        $groupManager = $this->get('fos_user.group_manager');
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.group.form.factory');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $group = $groupManager->createGroup('');

        $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_INITIALIZE, new GroupEvent($group, $request));

        $form = $formFactory->createForm();
        $form->setData($group);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_SUCCESS, $event);
            
            
            //$group->addUser($user);
            
            
            $groupManager->updateGroup($group);
            
           
            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_group_show', array('groupName' => $group->getName()));
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::GROUP_CREATE_COMPLETED, new FilterGroupResponseEvent($group, $request, $response));
            

            return $response;
        }

        return $this->render('FOSUserBundle:Group:new.html.twig', array(
            'form' => $form->createview(),
        ));
    }
    
    
    public function listAction()
    {
        $user = $this->getUser();
        
        $group = $this->get('fos_user.group_manager')->findGroupByName('supermariobros');
        
        $groupNames = $user->getGroupNames();
        
        dump($group);
        
        
        
        //$groups = $this->get('fos_user.group_manager')->findGroups();

        return $this->render('FOSUserBundle:Group:list.html.twig', array(
            'groups' => $group
        ));
    }
    
    
    
    public function addUserAction($groupName)
    {
        $group = $this->findGroupBy('name', $groupName);
        
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.group.form.factory');

        $form = $formFactory->createForm();
        $form->setData($group);
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getDoctrine()
        ->getRepository('RandomsoftVisionsourceBundle:User')
        ->findBy(array('username' => 'mcmuffinz'));
        
        $rolesArr = array('ROLE_'.$groupName.'_MEMBER');
        
        $user[0]->setRoles($rolesArr);
        
        dump($user[0]);
        dump($groupName);
        dump($group);
        
        return $this->render('FOSUserBundle:Group:edit.html.twig', array(
            'form'      => $form->createview(),
            'group_name'  => $group->getName(),
        ));
        
              
                 
        
    }
    
    
    
    
}