<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Randomsoft\VisionsourceBundle\Entity\Control;
use Randomsoft\VisionsourceBundle\Entity\Word;
use Randomsoft\VisionsourceBundle\Entity\Phrase;
use Randomsoft\VisionsourceBundle\Entity\Picture;
//use Randomsoft\VisionsourceBundle\Entity\Invitation;
//use Randomsoft\VisionsourceBundle\Entity\InviteEmail;
//use Randomsoft\VisionsourceBundle\Form\InviteFormType;
use Randomsoft\VisionsourceBundle\Form\ControlType;
use Randomsoft\VisionsourceBundle\Form\PictureType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class ControlController extends Controller{

    public function createAction(Request $request){
        
        //$this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'You are not authorized to access this feature!');
        
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER', 'ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        
        $control = new Control();
        $user = $this->getUser();
        $pictures = $control->getPicture();
       
        $form = $this->createForm(new ControlType(), $control);
        
        if ($request->isMethod('POST')) {
                $form->handleRequest($request);
                $formData = $form->getData();
                $files = $request->files->get('file');
                $words = $formData->getWords();
                $phrases = $formData->getPhrases();
                $validator = $this->get('validator');
                $errors = $validator->validate($control);

                if (count($errors) > 0) {
                    return $this->render('RandomsoftVisionsourceBundle:Control:control.html.twig', array(
                        'form' => $form->createView(),
                        'errors' => $errors
                    ));
                }else{

                    $em = $this->getDoctrine()->getManager();
                 
                    if($words){
                        foreach($words as $currentWord){
                            $word = new Word();
                            $word->setWord($currentWord);
                            $word->setCreator($user->getUsername());
                            $em->persist($word);
                            $em->flush();
                            $em->clear();
                        }  
                        
                    }
                    if($phrases){
                        foreach($phrases as $currentPhrase){
                            $phrase = new Phrase();
                            $phrase->setPhrase($currentPhrase);
                            $phrase->setCreator($user->getUsername());
                            $em->persist($phrase);
                            $em->flush();
                            $em->clear();
                        }  
                        
                    }
                    if($files){
                        foreach($files as $uploadedFile) {

                             $nameInit = '';
                             $picture = new Picture();
                             $originalName = $uploadedFile->getClientOriginalName();
                             $mimeType = $uploadedFile->getClientMimeType();
                             $fileName = $picture->generateName($mimeType, $nameInit);
                             $directory = __DIR__.'/../../../../web/bundles/randomsoftvisionsource/uploads/';
                             $picture->setFile($uploadedFile);
                             $picture->setName($fileName);
                             $picture->setCreator($user->getUsername());
                             $file = $uploadedFile->move($directory, $fileName);
                             $em->persist($picture);
                             $em->flush();
                             $em->clear();
                            
                            }
                            
                    }   
               return $this->redirect($this->generateUrl('RandomsoftVisionsourceBundle_control_panel'));
            }
        
        }
        
        return $this->render('RandomsoftVisionsourceBundle:Control:control.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    
    
    
}
    
    
