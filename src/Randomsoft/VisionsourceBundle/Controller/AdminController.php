<?php

namespace Randomsoft\VisionsourceBundle\Controller;

use Symfony\Component\Filesystem\Filesystem;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{
   
    
    public function viewAction()
    {
        // controllers extending the base AdminController can access to the
        // following variables:
        //   $this->request, stores the current request
        //   $this->em, stores the Entity Manager for this Doctrine entity

        // change the properties of the given entity and save the changes
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository('RandomsoftVisionsourceBundle:Picture')->find($id);
        $pictureName = $entity->getName(); 
        
        $picturePath = $this->get('kernel')->getRootDir() . '/../web/bundles/randomsoftvisionsource/uploads/' . $pictureName;

        return $this->redirect($picturePath);
    }
    
    
    
    
    public function preRemovePictureEntity($entity) 
    {  
        $fs = new Filesystem();
        $pictureName = $entity->getName(); 
        $picturePath = $this->get('kernel')->getRootDir() . '/../web/bundles/randomsoftvisionsource/uploads/' . $pictureName;
        $fs->remove(array($picturePath));
    }
    
    public function prePersistNotificationEntity($entity) 
    {  
        $user = $this->getUser();   
        if($entity->isSigned()) {
            $entity->setCreator($user->getUsername());
        }else{
            $entity->setCreator('Visionsource');
        }
    }
    
    public function preUpdateNotificationEntity($entity) 
    {  
        $user = $this->getUser();   
        if($entity->isSigned()) {
            $entity->setCreator($user->getUsername());
        }else{
            $entity->setCreator('Visionsource');
        }
    }
    
}