<?php
namespace Randomsoft\VisionsourceBundle\Form\DataTransformer;
         
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
 
class WordDataTransformer implements DataTransformerInterface
{
    
    public function transform($words)
    {
        if ($words === null) {
            return ''; // default value
        }
 
        return implode(',', $words); // concatenate the words to one string
    }
    
    public function reverseTransform($words)
    {
        if (!$words) {
             return; // default
        }
 
        return array_filter(array_map('trim', explode(',', $words)));
        // 1. Split the string with commas
        // 2. Remove whitespaces around the tags
        // 3. Remove empty elements 
    }
}