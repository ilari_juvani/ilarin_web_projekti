<?php
namespace Randomsoft\VisionsourceBundle\Form\DataTransformer;
 
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
 
class PhraseDataTransformer implements DataTransformerInterface
{
    
    public function transform($phrases)
    {
        if ($phrases === null) {
            return ''; // default value
        }
 
        return implode(';', $phrases); // concatenate the phrases to one string
    }
    
    public function reverseTransform($phrases)
    {
        if (!$phrases) {
            return; // default
        }
 
        return array_filter(array_map('trim', explode(';', $phrases)));
        // 1. Split the string with commas
        // 2. Remove whitespaces around the tags
        // 3. Remove empty elements 
    }
}