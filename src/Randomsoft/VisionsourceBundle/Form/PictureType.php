<?php
// src/Randomsoft/VisionsourceBundle/Form/PictureType.php
namespace Randomsoft\VisionsourceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;

/**
 *
 * @Service("randomsoft_visionsource.form.type.picture")
 * @Tag("form.type", attributes = {"alias" = "picture"})
 */
class PictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', array(
                        'required' => false,
                        'empty_data'  => null));
    }

                      
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Randomsoft\VisionsourceBundle\Entity\Picture'
        ));
    }
    

    public function getName()
    {
        return 'picture';
    }
}