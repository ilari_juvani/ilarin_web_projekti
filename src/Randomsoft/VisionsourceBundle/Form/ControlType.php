<?php

namespace Randomsoft\VisionsourceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Randomsoft\VisionsourceBundle\Form\DataTransformer\WordDataTransformer;
use Randomsoft\VisionsourceBundle\Form\DataTransformer\PhraseDataTransformer;
use Randomsoft\VisionsourceBundle\Entity\Picture;
use Randomsoft\VisionsourceBundle\Form\PictureType;


class ControlType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
        $builder->create('words', 'text', array(
                        'required' => false,
                        'empty_data'  => null)
                        )->addModelTransformer(new WordDataTransformer()
                    ));
        $builder->add(
        $builder->create('phrases', 'textarea', array(
                        'required' => false,
                        'empty_data'  => null)
                        )->addModelTransformer(new PhraseDataTransformer()
                    ));
        $builder->add('picture', 'collection', array(
                    'type' => new PictureType(),
                    'label' => false,
                    
                    ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Randomsoft\VisionsourceBundle\Entity\Control'
        ));
    }
    

    /**
     * @return string
     */
    public function getName()
    {
        return 'control';
    }
}
