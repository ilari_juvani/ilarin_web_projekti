<?php

namespace Randomsoft\VisionsourceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // get your entity related with your form type
        $userPicEntity = $builder->getForm()->getData();
        
        $builder->add('imageCroppedPath', 'comur_image', array(
                       'label' => false,
                       'uploadConfig' => array(
                            'uploadRoute' => 'comur_api_upload',        //optional
                            'uploadUrl' => $userPicEntity->getUploadRootDir(),       // required - see explanation below (you can also put just a dir path)
                            'webDir' => 'visionsource/web/uploads/user',              // required - see explanation below (you can also put just a dir path)
                            'fileExt' => '*.jpg;*.gif;*.png;*.jpeg',    //optional
                            'libraryDir' => null,                       //optional
                            'libraryRoute' => 'comur_api_image_library', //optional
                            'showLibrary' => false,                      //optional
                            'generateFilename' => true          //optional
                        ),
                        'cropConfig' => array(
                            'minWidth' => 400,
                            'minHeight' => 400,
                            'aspectRatio' => true,              //optional
                            'cropRoute' => 'comur_api_crop',    //optional
                            'forceResize' => false,             //optional
                            'thumbs' => array(                  //optional
                                array(
                                    'maxWidth' => 50,
                                    'maxHeight' => 50,
                                    'useAsFieldImage' => false  //optional
                                )
                            )
                        )
        ));
    }
/*
    public function getParent()
    {
        return 'fos_user_profile';
    }
*/
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Randomsoft\VisionsourceBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'visionsource_user_profile_edit';
    }
}