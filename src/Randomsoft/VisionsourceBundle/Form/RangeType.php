<?php

namespace Randomsoft\VisionsourceBundle\Form;

use Symfony\Component\Form\AbstractType;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;


class RangeType extends AbstractType
{
    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return 'integer';
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return 'range';
    }
} 