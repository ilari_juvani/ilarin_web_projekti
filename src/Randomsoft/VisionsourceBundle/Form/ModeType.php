<?php
// src/Randomsoft/VisionsourceBundle/Form/ModeType.php

namespace Randomsoft\VisionsourceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Randomsoft\VisionsourceBundle\Form\RangeType;

class ModeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('modeChoice', 'choice', array(
            'choices' => array('word' => 'Word', 'phrase' => 'Phrase', 'picture' => 'Picture' ),
            'expanded' => true,
            'multiple' => false,
            'label' => false
        ));
        
        
        $builder->add('interval', 'hidden');
        
    }

    public function getName()
    {
        return 'mode';
    }
}